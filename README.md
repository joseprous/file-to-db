# file-to-db

writes files to a postgresql database

# usage

- connection string in the environment variable DATABASE_URL
- write files: file-to-db w files_paths
- read files: file-to-db r files_paths

