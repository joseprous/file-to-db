{-# LANGUAGE OverloadedStrings #-}

module Application
  ( startApp,
  )
where

import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as BSU
import Data.Functor ((<&>))
import Data.Maybe
import Database.PostgreSQL.Simple
import System.Environment

getEnvBS :: String -> String -> IO BS.ByteString
getEnvBS var val =
  lookupEnv var <&> BSU.fromString . fromMaybe val

insertFile :: Connection -> String -> IO ()
insertFile conn filename =
  do
    filedata <- BS.readFile filename
    putStrLn $ "writing: " ++ (show $ BS.length filedata)
    execute
      conn
      "insert into files(file_name,file_data) values (?, ?) on conflict (file_name) do update set file_data = excluded.file_data"
      (filename, Binary filedata)
    return ()

loadFile :: Connection -> String -> IO ()
loadFile conn filename =
  do
    [Only filedata] <- query conn "select file_data from files where file_name = ?" $ Only filename
    BS.writeFile filename filedata

startApp :: IO ()
startApp = do
  (op : filenames) <- getArgs
  connectionString <- getEnvBS "DATABASE_URL" "postgres://files:files@localhost:5432/files"
  conn <- connectPostgreSQL connectionString
  case op of
    "w" -> mapM_ (insertFile conn) filenames
    "r" -> mapM_ (loadFile conn) filenames
